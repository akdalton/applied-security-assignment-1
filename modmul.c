#include "modmul.h"
#include "stdint.h"
#include "stdbool.h"
#include "fcntl.h"
#include "unistd.h"
#include "inttypes.h"
#include <math.h>

#define WIN_SIZE 8

/*
Perform stage 1:

- read each 3-tuple of N, e and m from stdin,
- compute the RSA encryption c, then
- write the ciphertext c to stdout.
*/

// functions for random number generation
void rdrand64(uint64_t* r){
	int file = open("/dev/urandom", O_RDONLY);
	read(file, r, sizeof(*r));
	close(file);
}

void PRNG(mpz_t r, mpz_t q){
	uint64_t seed;
	rdrand64(&seed);
	gmp_randstate_t r_state;

	gmp_randinit_default(r_state);
	gmp_randseed_ui(r_state,seed);
	mpz_urandomm(r,r_state,q);
}

// 2^k-ary-slide-1Exp algorithm
// implimentation of the sliding windowed exponentiation algorithm
void windExp(mpz_t t, mpz_t x, mpz_t y, mpz_t N){
	mpz_t tempOut;
	uint64_t l,u,window,diff,check;
	mpz_init(tempOut);

	mpz_t preComp[1 << WIN_SIZE];
	
	mpz_init(preComp[0]);
	mpz_set_ui(preComp[0],1);

	for(int j = 1; j < (1 << WIN_SIZE); j++){
		mpz_init(preComp[j]);
		mpz_mul(preComp[j],preComp[j-1],x);
		mpz_mod(preComp[j],preComp[j],N);
	}
	
	// t <- 0
	// i <- |y| - 1
	mpz_set_ui(tempOut,1);
	int i = mpz_sizeinbase(y,2) - 1;

	// while i >= 0 do
	while(i >= 0){
		// if yi = 0 then l <- i, u <- 0
		if(mpz_tstbit(y,i) == 0){
			l = i;
			u = 0;
		// else
		} else {
			// l <- max(i-k+1,0)
			if(i - WIN_SIZE + 1 > 0){
				l = i - WIN_SIZE + 1;
			} else {
				l = 0;
			}
			// while yi...l = 0 (mod 2) do l <- l + 1
			window = 0;
			for(int j = l; j <= i; j++){
				diff = j - l;
				if(mpz_tstbit(y,j)){
					window = window | (1 << diff);
				}
			}
			check = window%2;
			while(check == 0){
				l++;
				window = window >> 1;
				check = window%2;
			}
			// u <- yi...l
			u = window;
		}
		// t <- [2^(i-l+1)]t
		mpz_powm_ui(tempOut,tempOut,(1 << (i-l+1)),N);

		// if u != 0 then t <- t + T[floor((u-1)/2)]
		if(u != 0){
			mpz_mul(tempOut,tempOut,preComp[u]);
			mpz_mod(tempOut,tempOut,N);
		}
		// i <- l - 1
		i = l - 1;
	}
	// return t
	mpz_set(t,tempOut);
	mpz_clear(tempOut);
	for(int j = 0; j < (1 << WIN_SIZE); j++){
		mpz_clear(preComp[j]);
	}
}

void montPreComp(mpz_t w, mpz_t pSq, mpz_t N){
	mpz_t zero,b;
	mpz_init(b);
	mpz_init(zero);

	mpz_set_ui(b,2);
	mpz_pow_ui(b,b,64);

	mpz_set_ui(w,1);
	for(int i = 1; i < 64; i++){
		mpz_mul(w,w,w);
		mpz_mul(w,w,N);
		mpz_mod(w,w,b);
	}
	mpz_set_ui(zero,0);
	mpz_sub(w,zero,w);
	mpz_mod(w,w,b);

	mpz_set_ui(pSq,1);
	for(int i = 1; i <= 2 * mpz_size(N) * 64; i++){
		mpz_add(pSq,pSq,pSq);
		mpz_mod(pSq,pSq,N);
	}
	mpz_clear(zero);
	mpz_clear(b);
}

void montMul(mpz_t r, mpz_t x, mpz_t y, mpz_t w, mpz_t N){
	mpz_t temp, yi_x, ui_N, temp1, ui, b;
	mpz_t r0,yi,x0;
	mpz_init(temp);
	mpz_init(ui);
	mpz_init(yi_x);
	mpz_init(ui_N);
	mpz_init(temp1);
	mpz_init(b);
	mpz_init(r0);
	mpz_init(yi);
	mpz_init(x0);

	mpz_set_ui(temp,0);
	mpz_mod(x,x,N);
	mpz_mod(y,y,N);
	mpz_set_ui(b,2);
	mpz_pow_ui(b,b,64);
	mpz_mod(x0,x,b);

	for(int i = 0; i < mpz_size(N); i++){
		mpz_mod(r0,temp,b);
		mpz_set_ui(yi,mpz_getlimbn(y,i));
		mpz_mul(ui,yi,x0);
		mpz_add(ui,r0,ui);
		mpz_mul(ui,ui,w);
		mpz_mod(ui,ui,b);
		mpz_mul(yi_x, x, yi);
		mpz_mul(ui_N, N, ui);
		mpz_add(temp1, yi_x, ui_N);
		mpz_add(temp,temp,temp1);
		mpz_div(temp,temp,b);
	}

	if(mpz_cmp(temp,N) >= 0){
		mpz_sub(temp,temp,N);
	}

	mpz_set(r,temp);

	mpz_clear(temp);
	mpz_clear(yi_x);
	mpz_clear(ui_N);
	mpz_clear(b);
	mpz_clear(temp1);
	mpz_clear(r0);
	mpz_clear(yi);
	mpz_clear(x0);
}

void wrappedMontMul(mpz_t r, mpz_t x, mpz_t y, mpz_t N){
	mpz_t w, pSq, xhat, yhat, rhat, one;
	mpz_init(w);
	mpz_init(pSq);
	mpz_init(xhat);
	mpz_init(yhat);
	mpz_init(rhat);
	mpz_init(one);

	mpz_set_ui(one,1);

	montPreComp(w,pSq,N);
	montMul(xhat,x,pSq,w,N);
	montMul(yhat,y,pSq,w,N);
	montMul(rhat,xhat,yhat,w,N);
	montMul(r,rhat,one,w,N);

	mpz_clear(w);
	mpz_clear(pSq);
	mpz_clear(xhat);
	mpz_clear(yhat);
	mpz_clear(rhat);
	mpz_clear(one);
}

void stage1() {
	mpz_t N, e, m, c;
	mpz_init(N);
	mpz_init(e);
	mpz_init(m);
	mpz_init(c);
	while(1 == gmp_scanf("%ZX",N)){
		if(1 != gmp_scanf("%ZX",e)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",m)){
			abort();
		}
		// windowed exponentiation
		windExp(c,m,e,N);
		gmp_printf("%ZX\n",c);
	}
	mpz_clear(N);
	mpz_clear(e);
	mpz_clear(m);
	mpz_clear(c);
}

/*
Perform stage 2:

- read each 9-tuple of N, d, p, q, d_p, d_q, i_p, i_q and c from stdin,
- compute the RSA decryption m, then
- write the plaintext m to stdout.
*/

void stage2() {
	mpz_t N, d, p, q, dp, dq, ip, iq, c, m1, m2, m, h;
	mpz_init(N);
	mpz_init(d);
	mpz_init(p);
	mpz_init(q);
	mpz_init(dp);
	mpz_init(dq);
	mpz_init(ip);
	mpz_init(iq);
	mpz_init(c);
	mpz_init(m1);
	mpz_init(m2);
	mpz_init(m);
	mpz_init(h);

	while (1 == gmp_scanf("%ZX",N)){
		if(1 != gmp_scanf("%ZX",d)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",p)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",q)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",dp)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",dq)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",ip)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",iq)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",c)){
			abort();
		}
		// using Chinese Remainder Theorem
		// windowed exponentiation
		windExp(m1,c,dp,p);
		windExp(m2,c,dq,q);
		mpz_sub(h,m1,m2);
		// montgomery multiplication
		wrappedMontMul(h,h,iq,p);
		// mpz_mul(h,h,iq);
		// mpz_mod(h,h,p);
		mpz_mul(m,h,q);
		mpz_add(m,m,m2);
		gmp_printf("%ZX\n",m);
	}

	mpz_clear(N);
	mpz_clear(d);
	mpz_clear(p);
	mpz_clear(q);
	mpz_clear(dp);
	mpz_clear(dq);
	mpz_clear(ip);
	mpz_clear(iq);
	mpz_clear(c);
	mpz_clear(m);
	mpz_clear(m1);
	mpz_clear(m2);
	mpz_clear(h);

}

/*
Perform stage 3:

- read each 5-tuple of p, q, g, h and m from stdin,
- compute the ElGamal encryption c = (c_1,c_2), then
- write the ciphertext c to stdout.
*/

void stage3() {

  	mpz_t p, q, g, h, m;
	mpz_t r, c1, c2;
  	mpz_init(p);
  	mpz_init(q);
  	mpz_init(g);
  	mpz_init(h);
  	mpz_init(m);
	mpz_init(r);
	mpz_init(c1);
	mpz_init(c2);

  	while(1 == gmp_scanf("%ZX",p)){
		if(1 != gmp_scanf("%ZX",q)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",g)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",h)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",m)){
			abort();
		}
		// pseudo random number generation
		PRNG(r,q);
		// mpz_set_ui(r,1);
		// windowed exponentiation
		windExp(c1,g,r,p);
		windExp(c2,h,r,p);
		// montgomery multiplication
		wrappedMontMul(c2,c2,m,p);
		gmp_printf("%ZX\n",c1);
		gmp_printf("%ZX\n",c2);
		
	}
  	mpz_clear(p);
  	mpz_clear(q);
  	mpz_clear(g);
  	mpz_clear(h);
  	mpz_clear(m);
  	mpz_clear(r);
  	mpz_clear(c1);
  	mpz_clear(c2);
}

/*
Perform stage 4:

- read each 5-tuple of p, q, g, x and c = (c_1,c_2) from stdin,
- compute the ElGamal decryption m, then
- write the plaintext m to stdout.
*/

void stage4() {

    mpz_t p, q, g, x, c1, c2, m, pSq;
	mpz_t mhat, c2hat, one, w;
  	mpz_init(p);
  	mpz_init(q);
  	mpz_init(g);
  	mpz_init(x);
  	mpz_init(c1);
	mpz_init(c2);
	mpz_init(m);
	mpz_init(pSq);
	mpz_init(mhat);
	mpz_init(c2hat);
	mpz_init(one);
	mpz_init(w);

  	while(1 == gmp_scanf("%ZX",p)){
		if(1 != gmp_scanf("%ZX",q)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",g)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",x)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",c1)){
			abort();
		}
		if(1 != gmp_scanf("%ZX",c2)){
			abort();
		}
		// windowed exponentiation
		windExp(m,c1,x,p);
		mpz_invert(m,m,p);
		// montgomery multiplication
		wrappedMontMul(m,m,c2,p);
		gmp_printf("%ZX\n",m);
	}

	mpz_clear(p);
	mpz_clear(q);
	mpz_clear(g);
	mpz_clear(x);
	mpz_clear(c1);
	mpz_clear(c2);
	mpz_clear(m);
	mpz_clear(pSq);
	mpz_clear(mhat);
	mpz_clear(c2hat);
	mpz_clear(one);
	mpz_clear(w);
}

/*
The main function acts as a driver for the assignment by simply invoking
the correct function for the requested stage.
*/

int main( int argc, char* argv[] ) {
  if( 2 != argc ) {
    abort();
  }

  if     ( !strcmp( argv[ 1 ], "stage1" ) ) {
    stage1();
  }
  else if( !strcmp( argv[ 1 ], "stage2" ) ) {
    stage2();
  }
  else if( !strcmp( argv[ 1 ], "stage3" ) ) {
    stage3();
  }
  else if( !strcmp( argv[ 1 ], "stage4" ) ) {
    stage4();
  }
  else {
    abort();
  }

  return 0;
}
